

function register(e){
    event.preventDefault();
    // console.log('working');

    let email = document.getElementById('email').value;
    let password = document.getElementById('password').value;

    let user = {
        email: email,
        password: password,
    };

    let json = JSON.stringify(user);
    localStorage.setItem(email, json);
    console.log('user added');
    window.location.href="/login";

}

function loginFunc(e){
    event.preventDefault();
    
    let email = document.getElementById('email').value;
    let password = document.getElementById('password').value;
    let result = document.getElementById('result');

    let user = localStorage.getItem(email);
    let data = JSON.parse(user);
    console.log(data);

    if(user == null){
        result.innerHTML = 'Wrong email';
    }else if(email == data.email && password == data.password){
        result.innerHTML = 'logged in';
        window.location.href="/game";
        
    }else{
        result.innerHTML = 'Wrong password';
    }
}