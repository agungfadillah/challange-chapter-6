const express = require('express')
const app = express();
const port = 3000
const {Biodata} = require('./models')


app.use(express.urlencoded({extended: false}))
app.use(express.json())

app.use(express.static('public'))
app.use('/css', express.static(__dirname + 'public/css'))
app.use('/images', express.static(__dirname + 'public/images'))
app.use('/assets', express.static(__dirname + 'public/assets'))
app.use('/javascript', express.static(__dirname + 'public/javascript'))


app.set('views', './views')
app.set('view engine', 'ejs')

app.get("/home", (request, response) => {
    response.render('home')
})

app.get("/game", (request, response) => {
    response.render('game')
})

app.get("/register", (request, response) => {
    response.render('register')
})

app.get("/login", (request, response) => {
    response.render('login')
})

app.get("/admin", (request, response) => {
    response.render('admin')
})

app.get("/tabel", (request, response) => {
    response.render('tabel')
})



app.get('/biodata', async (req, res) => {

    const biodata = await Biodata.findAll()
    res.json(biodata)
})

app.get('biodata/:id', (req, res) => {
    Biodata.findOne({
        where: {
            id: req.params.id
        }
    }).then((biodata) => {
        res.json(biodata)
    })
})

app.post('/biodata', async(req, res) =>{
    const biodata = await Biodata.create({
        kode_user: req.body.kode_user,
        nama: req.body.nama,
        alamat: req.body.alamat,
        telp: req.body.telp,
        status: req.body.status,
        tgl_lahir: req.body.tgl_lahir,
    })
    res.status(201).json(biodata)
})

app.put('/biodata/:id', async(req, res) =>{
    const id = req.params.id

    const biodata = await Biodata.update({
        alamat: req.body.alamat,
        telp: req.body.telp,
        status: req.body.status
    }, {
        where: {
            id: id
        }
    })

    res.json({
        messege: 'Updated Biodata'
    })
})

app.delete('/biodata/:id', async (req, res) =>{
    const id = req.params.id

    const biodata = await Biodata.destroy({
        where: {
            id: id
        }
    })
    res.json({
        messege: 'Deleted Biodata'
    })
})


app.use((request, response, next ) => {
    console.log(`${request.method} ${request.url}`)
    next()
})

const authRouter = require("./routes/auth.js")
app.use("/auth", authRouter)


app.use((err, req, res, next) => {
    res.status(500).json({
        status: ' fail',
        errors: err.message
    })
})

app.use((req, res, next) => {
    res.status(404).json({
        status: 'fail',
        error: 'Are you lost?'
    })
})

app.listen(port, () => {
    console.log(`Web started at port : ${port}`)
})